import React from 'react';
import classes from './Car.css'
import PropTypes from 'prop-types'
import withClass from '../hoc/withClass'

class Car extends React.Component {

    constructor(props) {
        super(props)

        this.inputRef = React.createRef()
    }

    componentDidMount(){
        if (this.props.index === 1) {

            this.inputRef.current.focus()
        }
    }

    render(){
        console.log('Car render')

        const inputClasses = [classes.input]

        if (this.props.name !== '') {
            inputClasses.push(classes.green)
        } else {
            inputClasses.push(classes.red)
        }

        if (this.props.name.length > 4) {
            inputClasses.push(classes.bold)
        }



        return (
            <>
                <h3>Car name: {this.props.name}</h3>
                <p>Year: <strong>{this.props.year}</strong></p>
                <input
                    ref = {this.inputRef}// не видене в html
                    type="text"
                    onChange={this.props.onChangeName}
                    value={this.props.name}
                    className={inputClasses.join(' ')}
                />
                <button onClick={this.props.onDelete}>Delete</button>
            </>
        )
    }
}

Car.propTypes = {
    name: PropTypes.string.isRequired,// обязательный параметр
    year: PropTypes.string,
    index: PropTypes.number,
    onChangeName: PropTypes.func,
    onDelete: PropTypes.func
}


export default withClass(Car, classes.Car)