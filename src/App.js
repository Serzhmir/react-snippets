import React, { Component } from 'react';
import './App.scss';
import Car from './Car/Car';
import ErrorBoudary from './ErrorBoudary/ErrorBoudary';
import Counter from './Counter/Counter'


class App extends Component {

    constructor(props) {
        console.log('App constructor')
        super(props);
        this.state = {
            cars: [
                {
                    name: 'Ford',
                    year: '2018'
                },
                {
                    name: 'Audi',
                    year: '2017'
                },
                {
                    name: 'LADA',
                    year: '2019'
                }
            ],
            pageTitle: 'React Components',
            showCars: false
        }
    }


    onChangeName(name, index) {
        const car = this.state.cars[index]
        car.name = name
        const cars = [...this.state.cars]// напрямую менять state нельзя нужно склонировать
        cars[index] = car
        this.setState({
            cars: cars
        })
    }

    deleteHandler(index){
        const cars = this.state.cars.concat();
        cars.splice(index, 1)// метод splice удаляет елемент из массива (первый параметр индекс в массиве, второй количество удаляемых ел-тов)
        this.setState({
            cars: cars
        })
    }

    toggleCarsHandler = () => {
        this.setState({
            showCars: !this.state.showCars
        })
    }

    componentWillMount(){
        console.log('App componentWillUnmount')
    }

    componentDidMount(){
        console.log('App componentDidMount')
    }

    render() {
        console.log('App render')
        const divStyle = {
            textAlign: 'center'
        }


     let cars = null;

        if (this.state.showCars) {
            cars = this.state.cars.map((car, index) => {
                return(
                    <ErrorBoudary key={index}>
                    <Car
                        name={car.name}
                        index={index}
                        year={car.year}
                        onDelete={this.deleteHandler.bind(this, index)}
                        onChangeName={event => this.onChangeName(event.target.value, index)}
                    />
                    </ErrorBoudary>
                )
            })
        }

        return (
            <div  style={divStyle} className="App">
                {/*<h1 style={{color: 'green', fontSize: '70px', lineHeight: '1.3', margin: '0'}}>{this.state.pageTitle}</h1>*/}
                <h1>{this.props.title}</h1>

                <Counter/>
                <hr/>
                <button
                    style={{marginTop: 20}}
                    className={'AppButton'}
                    onClick={this.toggleCarsHandler}>Toggle cars</button>

                <div style={{
                    width: 400,
                    margin: 'auto',
                    paddingTop: '20px'
                }}> {cars}</div>

            </div>
        );
    }
}

export default App;
